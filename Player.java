import javafx.scene.shape.Circle;

public interface Player {
    public abstract void moveRight();

    public abstract void moveLeft();

    public abstract void moveUp();

    public abstract void moveDown();

    public abstract Position getPosition();

    public abstract Circle getBall();
}
